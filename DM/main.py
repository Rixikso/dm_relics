#  -*- coding: utf8 -*-
import json
import os.path
import codecs
import matplotlib.pyplot as plt
import mysql.connector
import re
import collections

my_user = 'root'
my_password = 'root'
my_host = '127.0.0.1'
json_path = "relics\\relics-json\\"

globalkeys = []
globalvalues = []

class Parser_json:
    @staticmethod
    def getAllProperties():
        numberOfJson = getNumberOfJsons(json_path)
        namesOfJsons = getNamesOfJsons(json_path)


        _json = {}
        maxValueofKeys = 0
        properties = {}

        for i in range(numberOfJson - 1):
            _json = decodeJsonByName(namesOfJsons[i])
            properties.update(_json)
            if _json["photos"]:
                print(namesOfJsons[i])
                print(_json)
                return
            if len(_json) > maxValueofKeys :
                 print( len(_json))
                 maxValueofKeys = len(_json)
            elif len(_json) < maxValueofKeys:
                 print(len(_json))

            #Progress indicator
            if i % 1000 == 0:
                print("progress: " + str(i))

        print("max value of keys: " + str(maxValueofKeys))
        return properties.keys()

def create_database(name):
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host)
    cursor = cnx.cursor()
    try:
        cursor.execute("DROP DATABASE IF EXISTS " + name)
        cursor.execute(
            "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(name))
        print "CREATE DATABASE " + name
    except mysql.connector.Error as err:
        print("Failed creating database: {}".format(err))
        exit(1)

    cursor.close()
    cnx.close()

def create_tables(cursor):

    #VOIVODESHIP
    cursor.execute("DROP DATABASE IF EXISTS voivodeship")
    cursor.execute("CREATE  TABLE IF NOT EXISTS voivodeship ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "voivodeship_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id),"
    "UNIQUE INDEX name_UNIQUE (voivodeship_name ASC) )"
    )

    #District
    cursor.execute("DROP TABLE IF EXISTS district")
    cursor.execute("CREATE TABLE IF NOT EXISTS district ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "voivodeship_id INT UNSIGNED NOT NULL ,"
    "district_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (voivodeship_id) REFERENCES voivodeship (id) )"
    )

    #commune
    cursor.execute("DROP TABLE IF EXISTS commune")
    cursor.execute("CREATE TABLE IF NOT EXISTS commune ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "district_id INT UNSIGNED NOT NULL ,"
    "commune_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (district_id) REFERENCES district (id) )"
    )

    #place
    cursor.execute("DROP TABLE IF EXISTS place")
    cursor.execute("CREATE TABLE IF NOT EXISTS place ("
    "id INT UNSIGNED NOT NULL ,"
    "commune_id INT UNSIGNED NOT NULL ,"
    "place_name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (commune_id) REFERENCES commune (id) )"
    )

    #relic
    cursor.execute("DROP TABLE IF EXISTS relic")
    cursor.execute("CREATE TABLE IF NOT EXISTS relic ("
    "id INT UNSIGNED NOT NULL ,"
    "nid_id INT UNSIGNED NULL ,"
    "identification VARCHAR(255) NULL,"
    "place_id INT UNSIGNED NULL ,"
    "common_name VARCHAR(255) NULL ,"
    "description TEXT NULL ,"
    "state ENUM('unchecked','checked','filled') NOT NULL ,"
    "latitude DOUBLE NULL ,"
    "longitude DOUBLE NULL ,"
    "dating_of_obj VARCHAR(255) NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (place_id) REFERENCES place (id) )"

    )


    #event
    cursor.execute("DROP TABLE IF EXISTS event")
    cursor.execute("CREATE TABLE IF NOT EXISTS event ("
    "id INT UNSIGNED NOT NULL ,"
    "name VARCHAR(255) NOT NULL ,"
    "date VARCHAR(255) NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL,"
    "PRIMARY KEY (id),"
    "FOREIGN KEY (relic_id )REFERENCES relic (id ))")


    #entry
    cursor.execute("DROP TABLE IF EXISTS entry")
    cursor.execute("CREATE TABLE IF NOT EXISTS entry ("
    "id INT UNSIGNED NOT NULL ,"
    "title VARCHAR(255) NOT NULL ,"
    "body TEXT NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (relic_id ) REFERENCES relic (id) )")

    #document
    cursor.execute("DROP TABLE IF EXISTS document")
    cursor.execute("CREATE TABLE IF NOT EXISTS document ("
    "id INT UNSIGNED NOT NULL ,"
    "name VARCHAR(255) NULL ,"
    "description TEXT NULL ,"
    "url VARCHAR(255) NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (relic_id ) REFERENCES relic (id) )")

    #
    #alert
    cursor.execute("DROP TABLE IF EXISTS alert")
    cursor.execute("CREATE TABLE IF NOT EXISTS alert ("
    "id INT UNSIGNED NOT NULL ,"
    "url VARCHAR(255) NULL ,"
    "author VARCHAR(255) NULL ,"
    "date_taken VARCHAR(255) NULL ,"
    "description TEXT NOT NULL ,"
    "state ENUM('new','in_progress','fixed') NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (relic_id ) REFERENCES relic (id) )")


    #category
    cursor.execute("DROP TABLE IF EXISTS category")
    cursor.execute("CREATE TABLE IF NOT EXISTS category ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "UNIQUE INDEX name_UNIQUE (name ASC) )"
    )

    #relic_category
    cursor.execute("DROP TABLE IF EXISTS relic_category")
    cursor.execute("CREATE TABLE IF NOT EXISTS relic_category ("
    "category_id INT UNSIGNED NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "FOREIGN KEY (category_id)  REFERENCES category (id ) ,"
    "FOREIGN KEY (relic_id)  REFERENCES relic (id ) ) ")

    #tag
    cursor.execute("DROP TABLE IF EXISTS tag")
    cursor.execute("CREATE TABLE IF NOT EXISTS tag ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "UNIQUE INDEX name_UNIQUE (name ASC) )"
    )

    #relic_tag
    cursor.execute("DROP TABLE IF EXISTS relic_tag")
    cursor.execute("CREATE TABLE IF NOT EXISTS relic_tag ("
    "tag_id INT UNSIGNED NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "FOREIGN KEY (tag_id)  REFERENCES tag (id ) ,"
    "FOREIGN KEY (relic_id)  REFERENCES relic (id ) ) ")

    #link_category
    cursor.execute("DROP TABLE IF EXISTS link_category")
    cursor.execute("CREATE TABLE IF NOT EXISTS link_category ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "name VARCHAR(255) NOT NULL ,"
    "PRIMARY KEY (id) ,"
    "UNIQUE INDEX name_UNIQUE (name ASC) )"
    )

    #link
    cursor.execute("DROP TABLE IF EXISTS link")
    cursor.execute("CREATE TABLE IF NOT EXISTS link ("
    "id INT UNSIGNED NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "category_id INT UNSIGNED NOT NULL ,"
    "kind ENUM('paper','url') NOT NULL ,"
    "name VARCHAR(255) NOT NULL ,"
    "url VARCHAR(255) NULL ,"
    "PRIMARY KEY (id) ,"
    "FOREIGN KEY (category_id) REFERENCES link_category(id) ,"
    "FOREIGN KEY (relic_id) REFERENCES relic(id) )")

    #photo
    cursor.execute("DROP TABLE IF EXISTS photo")
    cursor.execute("CREATE TABLE IF NOT EXISTS photo ("
    "id INT UNSIGNED NOT NULL AUTO_INCREMENT ,"
    "author VARCHAR(255) NULL ,"
    "date_taken VARCHAR(255) NULL ,"
    "alt_text VARCHAR(255) NULL ,"
    "url VARCHAR(255) NOT NULL ,"
    "full_width INT UNSIGNED NULL ,"
    "PRIMARY KEY (id) ,"
    "UNIQUE INDEX name_UNIQUE (url ASC) )")

    #relic_photo
    cursor.execute("DROP TABLE IF EXISTS relic_photo")
    cursor.execute("CREATE TABLE IF NOT EXISTS relic_photo ("
    "photo_id INT UNSIGNED NOT NULL ,"
    "relic_id INT UNSIGNED NOT NULL ,"
    "FOREIGN KEY (photo_id)  REFERENCES photo (id ) ,"
    "FOREIGN KEY (relic_id)  REFERENCES relic (id ) ) ")


def convert_date_of_obj(dating):
    if not dating:
        return dating
    if "XX" in dating:
        return 20
    if "XIX" in dating:
        return 19
    if "XVIII" in dating:
        return 18
    if "XVII" in dating:
        return 17
    if "XVI" in dating:
        return 16
    if "XV" in dating:
        return 15
    if "XIV" in dating:
        return 14
    if "XIII" in dating:
        return 13
    if "XII" in dating:
        return 12
    if "XI" in dating:
        return 11
    if "X" in dating:
        return 10
    if "IX" in dating:
        return 9
    if "VIII" in dating:
        return 8
    if "VII" in dating:
        return 7
    if "VI" in dating:
        return 6
    if "V" in dating:
        return 15
    if "IV" in dating:
        return 4
    if "III" in dating:
        return 3

    m = re.search('([0-9]{4})', dating)
    if m:
        dat = m.group(1)
        try:
            int(dat)
            w = (int(dat)/100) +1
            if w < 22:
                return w
        except ValueError:
            pass
    return dating

def json_to_database(_json,cursor):
 ### voivodeship ####
        if 'voivodeship_name' in _json.keys():

            cursor.execute("SELECT * FROM voivodeship where voivodeship_name = '"+_json['voivodeship_name']+"'")
            if len(cursor.fetchall()) == 0:
                cursor.execute(
                      "INSERT INTO voivodeship " +
                     "(voivodeship_name) "
                     "VALUES(%s )",
                     ( _json['voivodeship_name'],
                       ))
        else:
            #counter+=1
            return

        ### district ####
        if 'district_name' in _json.keys():
            cursor.execute("SELECT district.id FROM district INNER JOIN voivodeship ON district.voivodeship_id = voivodeship.id WHERE district_name = '"+_json['district_name']+"' "
                           "AND voivodeship_name = '"+_json['voivodeship_name']+"' ")
            if len(cursor.fetchall()) == 0:
                cursor.execute("SELECT id FROM voivodeship where voivodeship_name = '"+_json['voivodeship_name']+"'")
                id_voivodeship=cursor.fetchall()[0][0]

                cursor.execute(
                                  "INSERT INTO district " +
                                 "(district_name ,voivodeship_id) "
                                 "VALUES(%s,%s )",
                                 ( _json['district_name'],
                                   id_voivodeship,
                                   ))

        ### commune####
        if 'commune_name' in _json.keys():
            cursor.execute("SELECT commune.id FROM commune "
                           "Inner JOIN district ON commune.district_id = district.id "
                           "INNER JOIN voivodeship ON district.voivodeship_id = voivodeship.id "
                           "WHERE commune_name = '"+_json['commune_name']+"' "
                                                                          "AND district_name = '"+_json['district_name']+"' AND voivodeship_name = '"+_json['voivodeship_name']+"' ")
            if len(cursor.fetchall()) == 0:
                cursor.execute("SELECT district.id FROM district INNER JOIN voivodeship ON district.voivodeship_id = voivodeship.id WHERE district_name = '"+_json['district_name']+"' "
                           "AND voivodeship_name = '"+_json['voivodeship_name']+"' ")
                id_district=cursor.fetchall()[0][0]

                cursor.execute(
                          "INSERT INTO commune " +
                         "(commune_name ,district_id) "
                         "VALUES(%s,%s )",
                         ( _json['commune_name'],
                           id_district,
                           ))

        ### place ####
        if 'place_name' in _json.keys():
            cursor.execute("SELECT id FROM place where id = '"+str(_json['place_id'])+"'")
            if len(cursor.fetchall()) == 0:
                cursor.execute("SELECT commune.id FROM commune "
                           "Inner JOIN district ON commune.district_id = district.id "
                           "INNER JOIN voivodeship ON district.voivodeship_id = voivodeship.id "
                           "WHERE commune_name = '"+_json['commune_name']+"' "
                                                                          "AND district_name = '"+_json['district_name']+"' AND voivodeship_name = '"+_json['voivodeship_name']+"' ")
                id_commune_t=cursor.fetchall()

                id_commune=id_commune_t[0][0]


                cursor.execute(
                          "INSERT INTO place " +
                         "(place_name ,commune_id,id) "
                         "VALUES(%s,%s,%s )",
                         ( _json['place_name'],
                           id_commune,
                           _json['place_id'],
                           ))
        #relic
        cursor.execute(
                          "INSERT INTO relic " +
                         "(id ,nid_id,identification,place_id,common_name,description ,state,latitude,longitude,dating_of_obj) "
                         "VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                         ( _json['id'],
                           _json['nid_id'],
                           _json['identification'],
                           _json['place_id'],
                           _json['common_name'],
                           _json['description'],
                           _json['state'],
                           _json['latitude'],
                           _json['longitude'],
                           convert_date_of_obj(_json['dating_of_obj']),


                           ))

        #events
        if _json['events']:
            #print _json['events'][0]['id'],_json['events'][0]['date'],_json['events'][0]['name']
            for event in _json['events']:
                cursor.execute(
                  "INSERT INTO event" +
                 "(id,name,date,relic_id) "
                 "VALUES(%s,%s,%s,%s )",
                 ( event['id'],
                  event['name'],
                  event['date'],
                  _json['id'],
                  ))

        #entries
        if _json['entries']:
            for entry in _json['entries']:
                cursor.execute(
                  "INSERT INTO entry" +
                 "(id,title,body,relic_id) "
                 "VALUES(%s,%s,%s,%s )",
                 ( entry['id'],
                  entry['title'],
                  entry['body'],
                  _json['id'],
                  ))

        #documents
        if _json['documents']:
            #print _json['documents']
            for document in _json['documents']:
                cursor.execute(
                  "INSERT INTO document" +
                 "(id,name,description,url,relic_id) "
                 "VALUES(%s,%s,%s,%s,%s )",
                 ( document['id'],
                  document['name'],
                  document['description'],
                  document['url'],
                  _json['id'],
                  ))
        #aletrs
        if _json['alerts']:
            for alert in _json['alerts']:
                cursor.execute(
                  "INSERT INTO alert" +
                 "(id,url,author,date_taken,description,state,relic_id) "
                 "VALUES(%s,%s,%s,%s,%s,%s,%s  )",
                 (alert['id'],
                  alert['url'],
                  alert['author'],
                  alert['date_taken'],
                  alert['description'],
                  alert['state'],
                  _json['id'],
                  ))
        #links
        if _json['links']:
            for link in _json['links']:


                cursor.execute("SELECT * FROM link_category where name = '"+link['category']+"'")
                if len(cursor.fetchall()) == 0:
                    cursor.execute(
                    "INSERT INTO link_category " +
                    "(name) "
                    "VALUES(%s )",
                    ( link['category'],
                    ))
                cursor.execute("SELECT id FROM link_category where name = '"+link['category']+"'")
                category_id=cursor.fetchall()[0][0]
                cursor.execute(
                        "INSERT INTO link" +
                        "(id,url,name,kind,category_id,relic_id) "
                        "VALUES(%s,%s,%s,%s,%s,%s  )",
                            (link['id'],
                            link['url'],
                            link['name'],
                            link['kind'],
                            category_id,
                            _json['id']
                    ))
        #categories
        if _json['categories']:
            for category in _json['categories']:
                cursor.execute("SELECT * FROM category where name = '"+category+"' ")
                if len(cursor.fetchall()) == 0:
                    cursor.execute(
                    "INSERT INTO category " +
                    "(name) "
                    "VALUES(%s )",
                    ( category,
                    ))
                cursor.execute("SELECT id FROM category where name = '"+category+"'")
                category_id=cursor.fetchall()[0][0]
                cursor.execute(
                    "INSERT INTO relic_category" +
                    "(category_id,relic_id) "
                     "VALUES(%s,%s  )",
                            (category_id,
                            _json['id']
                    ))
        #tag
        if _json['tags']:
            for tag in _json['tags']:
                cursor.execute("SELECT * FROM tag where name = '"+tag+"'")
                if len(cursor.fetchall()) == 0:
                    cursor.execute(
                    "INSERT INTO tag " +
                    "(name) "
                    "VALUES(%s )",
                    ( tag,))
                cursor.execute("SELECT id FROM tag where name = '"+tag+"'")
                tag_id=cursor.fetchall()[0][0]
                cursor.execute(
                    "INSERT INTO relic_tag" +
                    "(tag_id,relic_id) "
                    "VALUES(%s,%s  )",
                            (tag_id,
                            _json['id']
                     ))
        #photo
        if _json['photos']:
            for photo in _json['photos']:
                cursor.execute("SELECT * FROM photo where url = '"+photo['file']['url']+"'")
                if len(cursor.fetchall()) == 0:

                    cursor.execute(
                    "INSERT INTO photo " +
                    "(author,date_taken,alt_text,url,full_width) "
                    "VALUES(%s,%s,%s,%s,%s )",
                    (
                        photo['author'],
                        photo['date_taken'],
                        photo['alternate_text'],
                        photo['file']['url'],
                        photo['file_full_width'],

                    ))
                cursor.execute("SELECT id FROM photo where url = '"+photo['file']['url']+"'")
                photo_id=cursor.fetchall()[0][0]
                cursor.execute(
                    "INSERT INTO relic_photo" +
                        "(photo_id,relic_id) "
                        "VALUES(%s,%s  )",
                            (photo_id,
                            _json['id']
                     ))
def insert_into_tables(cursor):
    numberOfJsons = getNumberOfJsons(json_path)
    namesOfJsons = getNamesOfJsons(json_path)

    print(numberOfJsons)
    counter = 0
    for i in range(numberOfJsons -1):
        if i % 1000 == 0:
            print("progress: " + str(i))

        _json = decodeJsonByName(namesOfJsons[i])
        json_to_database(_json,cursor)
        if(_json['descendants']):
            for relic in _json['descendants']:
                json_to_database(relic,cursor)
    print(counter)




def get_table(cursor, table_name):
    print " --- "+ table_name+ " --- "
    cursor.execute("SELECT * FROM "+table_name)
    for row in cursor.fetchall():
        print row

def decodeJsonByName(name):
    if os.path.isfile(name):
        with codecs.open(name) as data_file:
            data = json.load(data_file)
            data_file.close()

            return data

def getNamesOfJsons(path):
    namesOfJsons = []
    for file in os.listdir(path):
        if file.endswith(".json"):
            namesOfJsons.append(path + file)
    return namesOfJsons

def getNumberOfJsons(path):
    return len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))])

###### Main ########
# print(Parser_json.getAllProperties())


def main_create_database():
    db_name = "mama"

    # create_database(db_name)
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()


    # create_tables(cursor)
    insert_into_tables(cursor)
    #get_table(cursor, "voivodeship")
    #get_table(cursor, "district")
    #get_table(cursor, "commune")
    #get_table(cursor,"place")
    #get_table(cursor,"relic")
    #get_table(cursor,"events")
    #get_table(cursor,"documents")

    cnx.commit()
    cursor.close()
    cnx.close()

def statsForVoivodeship():
    db_name = "mama"
    sort_attribute = "voivodeship_id"
    table = "district"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()
    cursor.execute("SELECT voivodeship.id FROM voivodeship")
    IDS = cursor.fetchall()

    data1 = {}
    data2 = []
    for i in range(0,len(IDS)):
        cursor.execute("SELECT voivodeship_name,COUNT(voivodeship_name) AS voivodeship_name FROM relic "+
        "INNER JOIN place ON relic.place_id = place.id "+
        "INNER JOIN commune ON place.commune_id = commune.id "+
        "INNER JOIN district ON commune.district_id = district.id "+
        "INNER JOIN voivodeship ON district.voivodeship_id = voivodeship.id WHERE voivodeship.id="+str(IDS[i][0]))
        data = cursor.fetchall()
        data1[data[0][0]] = data[0][1]
    sortedDict = sorted(data1.items(), key=lambda x: x[1],reverse = True)
    keys = [x[0] for x in sortedDict]
    values = [x[1] for x in sortedDict]


    plt.xlabel("Voivodeship")
    plt.ylabel("Count")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()




def statsCategories():
    db_name = "mama"
    sort_attribute = "category"
    table = "district"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()
    cursor.execute("SELECT id FROM category")
    IDS = cursor.fetchall()

    data1 = {}
    data2 = []
    for i in range(0,len(IDS)):
        cursor.execute("SELECT category.name,COUNT(category.name) FROM relic_category "+
        "INNER JOIN category ON relic_category.category_id = category.id "+
        "WHERE category_id = "+str(IDS[i][0]))
        data = cursor.fetchall()
        data1[data[0][0]] = data[0][1]
    sortedDict = sorted(data1.items(), key=lambda x: x[1],reverse = True)
    keys = [x[0] for x in sortedDict]
    values = [x[1] for x in sortedDict]


    plt.xlabel("Category")
    plt.ylabel("Count")
    print values
    print keys
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=90)
    plt.tight_layout()
    plt.show()


    
def statsLinkCategories():
    db_name = "mama"
    sort_attribute = "category"
    table = "district"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()
    cursor.execute("SELECT id FROM link_category")
    IDS = cursor.fetchall()

    data1 = {}
    data2 = []
    for i in range(0,len(IDS)):
        cursor.execute("SELECT link_category.name,COUNT(link_category.name) FROM link "+
        "INNER JOIN link_category ON link.category_id = link_category.id "+
        "WHERE link_category.id = "+str(IDS[i][0]))
        data = cursor.fetchall()
        data1[data[0][0]] = data[0][1]
    sortedDict = sorted(data1.items(), key=lambda x: x[1],reverse = True)
    keys = [x[0] for x in sortedDict]
    values = [x[1] for x in sortedDict]


    plt.xlabel("Link_Category")
    plt.ylabel("Count")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()

def statsDating_of_obj():
    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()

    keys = []
    values = []
    for i in range(12,21):
        cursor.execute("select count(*) from relic where dating_of_obj = '"+str(i)+"'")
        key = cursor.fetchall()[0][0]
        values.append(key)
        keys.append(i)

    #print keys
    #print values
    plt.xlabel("Wiek")
    plt.ylabel("Ilosc zabytkow")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()

def statsState():
    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()

    keys = []
    values = []
    #State
    cursor.execute("select count(state) from relic where state='unchecked'")
    values.append(cursor.fetchall()[0][0])
    keys.append("unchecked")
    cursor.execute("select count(state) from relic where state='checked'")
    values.append(cursor.fetchall()[0][0])
    keys.append("checked")
    #cursor.execute("select count(state) from relic where state='filled'")
    #values.append(cursor.fetchall()[0][0])
    #keys.append("filled")

    plt.xlabel("State")
    plt.ylabel("Ilosc zabytkow")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()


def statsAlert():
    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()

    cursor.execute("SELECT count(relic_id) FROM alert")
    print "Wczytalem",cursor.fetchall()[0][0] ,"alarmow"
    cursor.execute("SELECT relic_id FROM alert")
    IDS = cursor.fetchall()

    a = []
    for i in IDS:
        a.append(i[0])

    counter=collections.Counter(a)
    values= counter.values()

    keys =counter.keys()

    print keys[values.index(sorted(values)[-1])]
    print keys[values.index(sorted(values)[-2])]
    print keys[values.index(sorted(values)[-3])]
    print keys[values.index(sorted(values)[-4])]
    print keys[values.index(sorted(values)[-5])]
    print "z" ,(len(values)) ,"zabytków"
    counter=collections.Counter(values)
    values= counter.values()
    keys =counter.keys()
    plt.xlabel("Ilosc alarmow")
    plt.ylabel("Ilosc zabytkow")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()

def statsAlertState():
    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()
    ##########  State ################
    keys = []
    values = []

    cursor.execute("select count(state) from alert where state='new'")
    values.append(cursor.fetchall()[0][0])
    keys.append("new")
    cursor.execute("select count(state) from alert where state='fixed'")
    values.append(cursor.fetchall()[0][0])
    keys.append("fixed")
    cursor.execute("select count(state) from alert where state='in_progress'")
    values.append(cursor.fetchall()[0][0])
    keys.append("in_progress")

    plt.xlabel("State")
    plt.ylabel("Ilosc zabytkow")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()

    cursor.execute("select count(state) from alert where author=''")
    print(cursor.fetchall()[0][0])



def statsEvent():
    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()

    cursor.execute("SELECT count(relic_id) FROM event")
    print "Wczytalem",cursor.fetchall()[0][0] ,"zdarzen(event)"
    cursor.execute("SELECT relic_id FROM event")
    IDS = cursor.fetchall()

    a = []
    for i in IDS:
        a.append(i[0])

    counter=collections.Counter(a)
    values= counter.values()

    keys =counter.keys()

    print keys[values.index(sorted(values)[-1])]
    print keys[values.index(sorted(values)[-2])]
    print keys[values.index(sorted(values)[-3])]
    print keys[values.index(sorted(values)[-4])]
    print keys[values.index(sorted(values)[-5])]
    print "z" ,(len(values)) ,"zabytków"
    counter=collections.Counter(values)
    values= counter.values()
    keys =counter.keys()
    plt.xlabel("Ilosc zdarzen(event)")
    plt.ylabel("Ilosc zabytkow")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()

def statsEntry():
    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()

    cursor.execute("SELECT count(relic_id) FROM entry")
    print "Wczytalem",cursor.fetchall()[0][0] ,"zdarzen(entry)"
    cursor.execute("SELECT relic_id FROM entry")
    IDS = cursor.fetchall()

    a = []
    for i in IDS:
        a.append(i[0])

    counter=collections.Counter(a)
    values= counter.values()

    keys =counter.keys()
    print keys[values.index(sorted(values)[-1])]
    print keys[values.index(sorted(values)[-2])]
    print keys[values.index(sorted(values)[-3])]
    print keys[values.index(sorted(values)[-4])]
    print keys[values.index(sorted(values)[-5])]
    print "z" ,(len(values)) ,"zabytków"
    print(len(keys))

    counter=collections.Counter(values)
    values= counter.values()
    keys =counter.keys()
    plt.xlabel("Ilosc zdarzen(entry)")
    plt.ylabel("Ilosc zabytkow")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()

def statsEventAlertEntry():

    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()

    a = []
    #Entry
    cursor.execute("SELECT relic_id FROM entry")
    IDS = cursor.fetchall()
    for i in IDS:
        a.append(i[0])

    #Event
    cursor.execute("SELECT relic_id FROM event")
    IDS = cursor.fetchall()
    for i in IDS:
        a.append(i[0])

    #Alert
    cursor.execute("SELECT relic_id FROM alert")
    IDS = cursor.fetchall()
    for i in IDS:
        a.append(i[0])


    counter=collections.Counter(a)
    values= counter.values()
    keys =counter.keys()

    print "z" ,(len(values)) ,"zabytków"
    print(len(keys))
    counter=collections.Counter(values)
    values= counter.values()
    keys =counter.keys()
    plt.xlabel("Ilosc zdarzen(Event,Alert,Entry)")
    plt.ylabel("Ilosc zabytkow")
    plt.bar(range(len(values)), values, align='center',)
    plt.xticks(range(len(keys)), keys, size='small',rotation=70)
    plt.tight_layout()
    plt.show()
def stats():
    db_name = "mama"
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()

    #Count relic
    cursor.execute("select count(*) from relic")
    print "Wczytalem",cursor.fetchall()[0][0],"relic"
    #Count place
    cursor.execute("select count(*) from place")
    print "Wczytalem",cursor.fetchall()[0][0],"place"
    #Count commune
    cursor.execute("select count(*) from commune")
    print "Wczytalem",cursor.fetchall()[0][0],"commune"
    #Count district
    cursor.execute("select count(*) from district")
    print "Wczytalem",cursor.fetchall()[0][0],"district"
    #Count voivodeship
    cursor.execute("select count(*) from voivodeship")
    print "Wczytalem",cursor.fetchall()[0][0],"voivodeship"
    #Count event
    cursor.execute("select count(*) from event")
    print "Wczytalem",cursor.fetchall()[0][0],"event"
    #Count entry
    cursor.execute("select count(*) from entry")
    print "Wczytalem",cursor.fetchall()[0][0],"entry"
    #Count alert
    cursor.execute("select count(*) from alert")
    print "Wczytalem",cursor.fetchall()[0][0],"alert"





def statistic():
     stats()
    # statsEventAlertEntry()
    # statsEntry()
    # statsEvent()
    # statsAlert()
    # statsAlertState()
    # statsState()
    # statsDating_of_obj()
    # statsForVoivodeship()
    # statsCategories()
    # statsLinkCategories()

def database_create_insert():
    db_name = "mama"
    create_database(db_name)
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
    cursor = cnx.cursor()
    create_tables(cursor)
    insert_into_tables(cursor)
    cnx.commit()
    cursor.close()
    cnx.close()

#database_create_insert() #Jeśli nie mamy bazy lub chcesz przeładować obecną
statistic()
