<?php
	$id = $_POST['id'];
	$id_voivo= $_POST['id_voivo'];
	$DB_NAME = 'mama';
	$DB_HOST = 'localhost';
	$DB_USER = 'root';
	$DB_PASS = 'root';
	
	$conn  = mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME) or die("Error " . mysqli_error($conn));
	
	$query = 'SELECT relic.id, longitude, latitude, identification,state FROM relic_category 
			INNER JOIN relic ON relic_category.relic_id = relic.id
			INNER JOIN place ON relic.place_id = place.id
			INNER JOIN commune ON place.commune_id = commune.id
			INNER JOIN district ON commune.district_id = district.id
			INNER JOIN voivodeship ON district.voivodeship_id = voivodeship.id
			WHERE voivodeship.id= '. $id_voivo . ' AND category_id ='. $id;
	
	$result = mysqli_query($conn, $query) or die("Error in Selecting " . mysqli_error($conn));
	$table = array();
	
	while($row = $result->fetch_assoc()){
		$temp = array();
		 
		 
		//Values
		$temp = array('lon' => (float)$row['longitude'], 'lat' => (string)$row['latitude'], 'title' => (string)$row['identification'],'id' => (float)$row['id'],'state' => (string)$row['state']);
		$rows[] = $temp;
    }
	$result->free();
	$table['points'] = $rows;
	mysqli_close($conn);
	$jsonTable = json_encode($table, true);
	header("Content-Type: application/json");
	echo $jsonTable;
	$fp = fopen('result.json', 'w');
	
	fwrite($fp,$jsonTable);
	fclose($fp);
?>